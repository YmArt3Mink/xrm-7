﻿
namespace MRX_7.Machine
{
    public class Document
    {
        public string Name { get; set; }

        public Document()
        {
        }

        public Document(string name)
        {
            Name = name;
        }

        public Document Print()
        {
            System.Console.WriteLine($"Document: '{Name}' has been printed");
            return new Document();
        }
    }
}
