﻿using MRX_7.Machine.States;

namespace MRX_7.Machine
{
    public class CopyMachine
    {
        public const float PRINT_COST = 1.2F;

        public IMachineState State           { get; set; }
        public float         Balance         { get; set; }
        public DeviceType    DevType         { get; set; }
        public Document      DocumentToPrint { get; set; }

        public CopyMachine()
        {
            State = new InitState();
            Balance = 0F;
        }

        public void InsertCash(int amount)
        {
            State.InsertCash(this, amount);
        }

        public void SelectDevice(DeviceType devType)
        {
            State.SelectDevice(this, devType);
        }

        public Document PrintDocument()
        {
            return State.PrintDocument(this);
        }
        
        public void SelectDocument(Document doc)
        {
            State.SelectDocument(this, doc);
        }

        public float EjectChange()
        {
            return State.EjectChange(this);
        }
    }
}
