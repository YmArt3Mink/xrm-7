﻿
namespace MRX_7.Machine
{
    public interface IMachineState
    {
        void InsertCash(CopyMachine machine, int amount);
        void SelectDevice(CopyMachine machine, DeviceType devType);
        void SelectDocument(CopyMachine machine, Document doc);
        Document PrintDocument(CopyMachine machine);
        float EjectChange(CopyMachine machine);
    }
}
