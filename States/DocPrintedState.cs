﻿using System;

namespace MRX_7.Machine.States
{
    internal class DocPrintedState
        : StateBase
    {
        public override Document PrintDocument(CopyMachine machine)
        {
            throw new Exception("Please select new document");
        }

        public override void SelectDevice(CopyMachine machine, DeviceType devType)
        {
        }

        public override void SelectDocument(CopyMachine machine, Document doc)
        {
            machine.DocumentToPrint = doc;
            machine.State = new PrintDocState();
        }
    }
}
