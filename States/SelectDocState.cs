﻿using System;

namespace MRX_7.Machine.States
{
    internal class SelectDocState
        : StateBase
    {
        public override Document PrintDocument(CopyMachine machine)
        {
            throw new Exception("Please follow the use-case steps");
        }

        public override void SelectDevice(CopyMachine machine, DeviceType devType)
        {
        }

        public override void SelectDocument(CopyMachine machine, Document doc)
        {
            machine.DocumentToPrint = doc;
            machine.State = new PrintDocState();
        }
    }
}
