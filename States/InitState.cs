﻿using System;

namespace MRX_7.Machine.States
{
    internal class InitState
        : StateBase
    {
        public override void InsertCash(CopyMachine machine, int amount)
        {
            machine.Balance += amount;
            machine.State = new SelectDeviceState();
        }

        public override void SelectDevice(CopyMachine machine, DeviceType devType)
        {
            throw new NotImplementedException();
        }

        public override Document PrintDocument(CopyMachine machine)
        {
            throw new Exception("Please follow the use-case steps");
        }

        public override void SelectDocument(CopyMachine machine, Document doc)
        {
        }
    }
}
