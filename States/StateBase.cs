﻿
namespace MRX_7.Machine.States
{
    internal abstract class StateBase
        : IMachineState
    {
        public virtual void InsertCash(CopyMachine machine, int amount)
        {
            machine.Balance += amount;
        }
        
        public abstract void SelectDevice(CopyMachine machine, DeviceType devType);

        public abstract void SelectDocument(CopyMachine machine, Document doc);

        public abstract Document PrintDocument(CopyMachine machine);

        public float EjectChange(CopyMachine machine)
        {
            System.Console.WriteLine($"There is your spare change: {machine.Balance}");

            var result = machine.Balance;
            machine.Balance = 0;

            machine.State = new InitState();
            return result;
        }
    }
}
