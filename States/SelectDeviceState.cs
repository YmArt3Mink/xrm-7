﻿using System;

namespace MRX_7.Machine.States
{
    internal class SelectDeviceState
        : StateBase
    {
        public override void SelectDevice(CopyMachine machine, DeviceType devType)
        {
            machine.DevType = devType;
            machine.State = new SelectDocState();
        }

        public override Document PrintDocument(CopyMachine machine)
        {
            throw new Exception("Please follow the use-case steps");
        }

        public override void SelectDocument(CopyMachine machine, Document doc)
        {
            throw new Exception("Please follow the use-case steps");
        }
    }
}
