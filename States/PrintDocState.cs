﻿
namespace MRX_7.Machine.States
{
    internal class PrintDocState
        : StateBase
    {
        public override Document PrintDocument(CopyMachine machine)
        {
            machine.State = new DocPrintedState();
            return 
                machine
                    .DocumentToPrint
                    .Print()
                    ;
        }

        public override void SelectDevice(CopyMachine machine, DeviceType devType)
        {
        }

        public override void SelectDocument(CopyMachine machine, Document doc)
        {
            machine.DocumentToPrint = doc;
        }
    }
}
